package chhemro.tp01;//ex01.1

/**
 * Gr 5-lundi 9h50 Classe Etudiant contenant les methodes nessaires pour implimenter,
 * cherhcer et afficher les infomations nessaires pour un certain etudiant
 * 
 * @author RothmonyChhem EmileCoveney
 */
public class Etudiant {
	private String nom, prenom, adresse, matricule;
	private char sexe;
	private int hTutorat;
	private double note;
	private Enseignant enseignant;

	public Etudiant() {

	}

	/**
	 * Repartir les infomations qu'on a sur un etudiant
	 * 
	 * @param nom       Le nom de famille d un etudiant
	 * @param prenom    Le prenom de l etudiant
	 * @param sexe      Le sexe de l eleve represente par un char
	 * @param adresse   l'adresse ou l eleve habite
	 * @param matricule le code mattricule de 6 chiffre propre a l etudiant
	 * @param note      la note obtenue par l etudiant
	 * @param hTutorat  le temps de tutorat a completer si le eleve a eu une cote
	 *                  equivalent a E
	 * @param enseignat l enseignat attribuer a l eleve
	 * 
	 */
	public Etudiant(String nom, String prenom, char sexe, String adresse, String matricule, double note, int hTutorat,
			Enseignant enseignant) {
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.adresse = adresse;

		setMatricule(matricule);
		setNote(note);

		this.hTutorat = hTutorat;
		this.enseignant = enseignant;

	}

	/**
	 * affichage de l infomation demander sur un eleve
	 */
	public void afficher() {
		System.out
				.println("M-" + matricule + ", " + nom + prenom + ", " + note + ", Enseignant : " + enseignant.getNom());
	}

	/**
	 * evalue si l eleve a une note de passage
	 * 
	 * @param note la note obtenue par l eleve
	 * 
	 * @return le resultat obtenu sois reussi ou echec
	 */
	public String reussir(double note) {
		String res;
		if (note >= 60) {
			res = "reussi";

		} else {
			res = "echec";
		}
		return res;
	}

	/**
	 * attribue une lettre(cote) correspondant a la note obtenue et l interval de
	 * point
	 * 
	 * @param note la note obtenue dun etudiant lors de l examen
	 * 
	 * @return la lettre approprie a la note
	 */
	public char cote(double note) {
		char cote;
		if (note >= 90) {
			cote = 'A';
		} else if (note >= 70) {
			cote = 'B';
		} else if (note >= 60) {
			cote = 'C';
		} else if (note >= 40) {
			cote = 'D';
		} else {
			cote = 'E';
		}
		return cote;
	}

	/**
	 * si l eleve a eu une cote equivalent a E, il a besoin du tutorat donc cacul le
	 * temps de tutorat nessaire
	 * 
	 * @param nbh  le temps de tutorat a rajoute si cote est E
	 * @param cote afin savoir si on est ellible aux tutorat on doit savoir sa cote
	 *             eu sur l examen
	 * 
	 */
	public void tutoyer(int nbh, char cote) {
		if (cote == 'E') {

			hTutorat = hTutorat + nbh;
		}

	}

	/**
	 * chercher la Valeur de hTutorat de Etudiant
	 * 
	 * @return la valeur de temps de tutorat disponible
	 */
	public int getHTutorat() {
		return hTutorat;
	}

	/**
	 * initialise la Valeur hTutorat a hTutorat de l Etudiant
	 * 
	 * @param hTutorat la valuer de hTutorat que l etudiantX a
	 */
	public void setHTutorat(int hTutorat) {
		this.hTutorat = hTutorat;
	}

	/**
	 * chercher la matricule de Etudiant
	 * 
	 * @return la matricule de l eleve
	 */
	public String getMatricule() {
		return matricule;
	}

	/**
	 * initialise la Valeur matricule a la matricule de l Etudiant
	 * 
	 * @param matricule la matricule de l etudiantX
	 */
	public void setMatricule(String matricule) {
		if (matricule.length() == 6) {
			this.matricule = matricule;
		} else {
			System.out.println("Matricule n'est pas valide");
		}
	}

	/**
	 * chercher la note a Etudiant
	 * 
	 * @return la note de l eleve
	 */
	public double getNote() {
		return note;
	}

	/**
	 * initialise la Valeur note a la note de Etudiant ainsi verifier si la note est
	 * entre 0 et 100 si faux afficher erreur
	 * 
	 * @param note la note de l etudiantX
	 */
	public void setNote(double note) {
		if (note >= 0 && note <= 100) {
			this.note = note;
		} else {
			System.out.println("Note est invalide");
		}

	}

	/**
	 * chercher le sexe de l Etudiant
	 * 
	 * @return le sexe de l eleve
	 */
	public char getSexe() {
		return sexe;
	}

	/**
	 * initialise la Valeur sexe a sexe Etudiant
	 * 
	 * @param note la note de l etudiantX
	 */
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	/**
	 * chercher le nom de l Etudiant
	 * 
	 * @return l nom de l eleve
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * initialise la Valeur nom a nom de l Etudiant
	 * 
	 * @param nom le nom de l etudiantX
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * chercher le prenom de l Etudiant
	 * 
	 * @return le prenom de l eleve
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * initialise la Valeur prenom a prenom de l Etudiant
	 * 
	 * @param prenom la prenom de l etudiantX
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * chercher l adresse de l Etudiant
	 * 
	 * @return l adresse de l eleve
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * initialise la Valeur adresse a l adresse de l Etudiant
	 * 
	 * @param note la note de l etudiantX
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * chercher l enseignat attribuer a l Etudiant
	 * 
	 * @return l enseignant de l eleve
	 */
	public Enseignant getEnseignant() {
		return enseignant;
	}

	/**
	 * initialise la Valeur enseignant a enseignant de l Etudiant
	 * 
	 * @param enseignant la note de l etudiantX
	 */
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

}
