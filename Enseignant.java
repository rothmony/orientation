package chhemro.tp01;//ex01.1

/**
 * Gr 5-lundi 9h50-Class Enseignat contenant les methodes nessaires pour
 * implimenter, cherhcer et afficher les infomations nessaires pour un certain
 * enseignant
 * 
 * @author RothmonyChhem EmileCoveney
 */
public class Enseignant {
	private String nom, matricule;

	public Enseignant() {

	}

	/**
	 * Repartir les infomations qu'on a sur un enseignant
	 * 
	 * @param nom       Le nom complet d un enseignant
	 * @param matricule le code mattricule de 6 chiffre propre a l enseignant
	 * 
	 */
	public Enseignant(String nom, String matricule) {
		this.nom = nom;
		setMatricule(matricule);
	}

	/**
	 * affichage de l infomation demander sur un eleve
	 */
	public void afficher() {
		System.out.println("M-" + matricule + ", " + nom);
	}

	/**
	 * chercher la matricule de Enseignant
	 * 
	 * @return la matricule de l enseignant
	 */
	public String getMatricule() {
		return matricule;
	}

	/**
	 * initialise la Valeur matricule a la matricule de l enseignant
	 * 
	 * @param matricule la matricule de l enseignantX
	 */
	public void setMatricule(String matricule) {
		if (matricule.length() == 6) {
			this.matricule = matricule;
		} else {
			System.out.println("Matricule n'est pas valide");
		}
	}

	/**
	 * chercher le nom de l Enseignant
	 * 
	 * @return l nom de l enseignant
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * initialise la Valeur nom a nom de l Enseignant
	 * 
	 * @param nom le nom de l enseignantX
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
}
