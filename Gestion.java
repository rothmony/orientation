package chhemro.tp01;//ex01.2

/**
 * Gr 5-lundi 9h50-Class affichant procedure/manipulation demander afficher l
 * information correspondent a la methode affiche() qui affiche certaines
 * infomations pour les enseignants et les etudiants, la resussite des eleves, le
 * nombre d heure de tutorat
 * 
 * @author RothmonyChhem EmileCoveney
 */
public class Gestion {// question2
	public static void main(String[] args) {
		// init enseignant
		Enseignant enseignant1 = new Enseignant("Emir Alami", "234567");
		Enseignant enseignant2 = new Enseignant("Ines Salmi", "234566");

		// init etudiant
		Etudiant etudiant1 = new Etudiant("Anas ", "Bouclair ", 'M', "6646 rue Queen", "670557", 56, 0, enseignant1);
		Etudiant etudiant2 = new Etudiant("Tremblant ", "Alma ", 'F', "12 rue hubert", "118910", 31, 0, enseignant2);
		Etudiant etudiant3 = new Etudiant("Beauchamp ", "Ilyane ", 'F', "78 rue reines", "670558", 80, 0, enseignant1);

		// affiche info enseignants
		enseignant1.afficher();
		enseignant2.afficher();
		etudiant1.afficher();

		// affiche info etudiants
		etudiant2.afficher();
		etudiant3.afficher();
		enseignant1.getNom();

		// affiche nom et reussite de l eleve
		System.out.println(etudiant1.getPrenom() + etudiant1.getNom() + " : " + etudiant1.reussir(etudiant1.getNote()));
		System.out.println(etudiant2.getPrenom() + etudiant2.getNom() + " : " + etudiant2.reussir(etudiant2.getNote()));
		System.out.println(etudiant3.getPrenom() + etudiant3.getNom() + " : " + etudiant3.reussir(etudiant3.getNote()));

		// appel et affiche le nombre d heure de tutorat pour chaque eleve
		etudiant1.tutoyer(5, etudiant1.cote(etudiant1.getNote()));
		System.out.println(etudiant1.getNom() + etudiant1.getHTutorat());
		etudiant2.tutoyer(5, etudiant2.cote(etudiant2.getNote()));
		System.out.println(etudiant2.getNom() + etudiant2.getHTutorat());
		etudiant3.tutoyer(5, etudiant3.cote(etudiant3.getNote()));
		System.out.println(etudiant3.getNom() + etudiant3.getHTutorat());

	}

}
